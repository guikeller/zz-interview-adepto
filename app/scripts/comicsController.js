'use strict';

angular.module('marvelApp.comicsController', [])

    .controller('ComicsController', ['$scope', 'MarvelAPI', '_', function ($scope, MarvelAPI, _) {
        $scope.formData = {};

        $scope.searchComics = function () {
            $scope.comics = [];

            var characterIds = [];
            MarvelAPI.findCharacter($scope.formData.name)
                .then(function (response) {
                    // Getting and storing the character ids
                    _.each(response.characters, function (data) {
                        characterIds.push(data.id);
                    });

                    // Can now find the Comics
                    if (characterIds != null && characterIds.length > 0) {
                        MarvelAPI.getComics(characterIds)
                            .then(function (response) {
                                    if (response) {
                                        _.each(response.comics, function (data) {
                                                $scope.comics.push(extractValues(data));
                                            }
                                        );
                                        $scope.searchChar = 'success';
                                    }
                                }, function (error) {
                                    $scope.errorMsg = error.message;
                                    $scope.searchChar = 'fail';
                                }
                            );
                    }
                }, function (error) {
                    $scope.errorMsg = error.message;
                    $scope.searchChar = 'fail';
                });
        };
    }]);

// Helper
function extractValues(data){
    var values = {
        'title': data.title,
        'url': data.urls[0].url,
        'desc': data.description != null ? data.description : 'Click the link above for more info..'
    };
    return values;
}