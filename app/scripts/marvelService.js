/**
 * Created by guikeller on 15/07/17.
 */
'use strict';

angular.module('marvelApp.marvelService', [])

    .service('MarvelAPI', function($http, $q) {
        var apiUrl = 'http://gateway.marvel.com/v1/public/';
        var apiKey = 'a2bab970f9012828957d59f8a1d61092';
        var privateKey = '29119ba3ea3a756b7f88db03c16fa6b10189b704';

        // Get Comics
        this.getComics = function(charactersId) {
            var def = $q.defer();

            var timestamp = new Date().getTime();
            var hash = CryptoJS.MD5(timestamp + privateKey + apiKey);
            var url = apiUrl + 'comics?characters=' + charactersId + '&apikey=' + apiKey + '&ts=' + timestamp + '&hash='+hash;
            console.log('comicsService :: getComics : '+url);

            $http.get(url)
                .success(function (response) {
                    if (response.data.results.length > 0) {
                        def.resolve({
                            comics: response.data.results
                        });
                    }
                    else {
                        def.reject({
                            message: 'Unable to find comics using the given filter'
                        });
                    }
                }).error(function () {
                def.reject({
                    message: 'Oops, API error - please contact us!'
                });
            });
            return def.promise;
        };

        // Find Character
        this.findCharacter = function(character){
            var def = $q.defer();

            var timestamp = new Date().getTime();
            var hash = CryptoJS.MD5(timestamp + privateKey + apiKey);
            var url = apiUrl + 'characters?name=' + character + '&apikey=' + apiKey + '&ts=' + timestamp + '&hash='+hash;

            console.log('comicsService :: findCharacter : '+url);

            $http.get(url)
                .success(function (response) {
                    if (response.data.results.length > 0) {
                        def.resolve({
                            characters: response.data.results
                        });
                    }
                    else {
                        def.reject({
                            message: 'Unable to find characters using the given filter'
                        });
                    }
                }).error(function () {
                def.reject({
                    message: 'Oops, API error - please contact us!'
                });
            });
            return def.promise;
        }

    });