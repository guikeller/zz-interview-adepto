'use strict';

// Declare app level module which depends on views, and components
angular.module('marvelApp', [
    'ngRoute',
    'angular-loading-bar',
    'marvelApp.underscore',
    'marvelApp.marvelService',
    'marvelApp.comicsController'
])
    .config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.when('/marvel/comics', {
            templateUrl: '/marvel/comics.html',
            controller: 'ComicsController'
        });

        $routeProvider.otherwise({redirectTo: '/marvel/comics'});
    }]);
