angular.module('marvelApp.underscore', [])
    .factory('_', [function() {
        // assumes underscore has already been loaded on the page
        return window._;
    }]);
